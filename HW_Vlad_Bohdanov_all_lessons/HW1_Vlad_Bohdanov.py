# 1 Задача: Створіть дві змінні first=10, second=30. Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.

first = 10
second = 30

third = first + second # 40
print(third)

third = first - second # -20
print(third)

third = first / second # 0.3333333333333333
print(third)

third = first // second # 0
print(third)

third = first % second # 10
print(third)

third = first * second # 300
print(third)

third = first ** second # 1000000000000000000000000000000

print(third)

# 2 Задача: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1. Виведіть на екран результат кожного порівняння.

comparison = first == second
print(comparison)

comparison = first > second
print(comparison)

comparison = first < second
print(comparison)

comparison = first != second
print(comparison)

comparison = first >= second
print(comparison)

comparison = first <= second
print(comparison)

# 3 Задача: Створіть змінну - результат конкатенації строк "Hello " та "world!".


word1 = 'Hello '
word2 = 'world!'

text = word1 + word2
print(text)

tpl = '{word1_value}{word2_value}'
text = tpl.format(word1_value=word1, word2_value=word2)
print(text)

text = '%s%s' % (word1, word2)
print(text)

text = '{}{}'.format(word1, word2)
print(text)

text = f'{word1}{word2}'
print(text)

#Зробив декілька варіантів для себе ^^

# Extra task
# 1. Навчитись автоматизувати свою мануальну роботу на посаді QA Enginner. Є ціль стати General QA у майбутньому.
# 2. Наразі я у свої компанії знаходжусь на бенчі, тож хотілось би пройти на проект (коли його знайдуть) де необхідний Genral QA.
# 3. Наразі готовий витрачати майже увесь вільний час (Зараз поки на бенчі робота 4год на день).